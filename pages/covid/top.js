import React from "react";
import { Doughnut } from 'react-chartjs-2';
import toNum from '../../helpers/toNum';

export default function Top({data}) {

    console.log(data)
    const countriesStats = data.countries_stat;
    // console.log(countriesStats)
    const countriesCases = countriesStats.map(country => {
        return {
            name: country.country_name,
            cases: toNum(country.cases)
        }
    })
    console.log(countriesCases);
    ///////////////////////////////////////////////////////////
    const countries = countriesCases.slice(0, 10);
    console.log(countries);
    ///////////////////////////////////////////////////////////
    const cases = countries.map(country => {
        return country.cases
    })
    const label = countries.map(country => {
        return country.name
    })
    console.log(cases);
    console.log(label);
    ////////////////////////////////////////////////////////////

    return(
        <React.Fragment>
            <h1>Top 10 Countries with the highest number of Cases</h1>
                <Doughnut 
                data={{
                    datasets: [{
                        data: cases,
                        backgroundColor: ['red', 'orange', 'yellow', 'blue', 'green', 'purple', 'black', 'bluegreen', 'yellowgreen', 'indigo']
                    }],
                    labels: label
                }}
                redraw={ false }
            />
        </React.Fragment>
    )

}

export async function getStaticProps() {
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
            "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })
    const data = await res.json();

    return {
        props: {
            data
        }
    }
}

// datasets: [{
//     data: [ countries.cases[0], countries.cases[1] ],
//     backgroundColor: ['red', 'orange' ]
// }],
// labels: [countries.name[0], countries.name[1] ]

// const countriesStats = data.countries_stat;
// const arr =  countriesStats 

// const result = arr.map(country => {
//     return ({
//         name: country.country_name,
//         cases: country.cases //need toNum
//     })
// });

// console.log(result)
// return(
//     <React.Fragment>
//         {result}
//     </React.Fragment>
// )