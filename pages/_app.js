// base imports
import React from 'react';
import { Container } from 'react-bootstrap';

// style imports
import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// app components
import NaviBar from '../components/NaviBar';


function MyApp({ Component, pageProps }) {
    return (
        <React.Fragment>
            <NaviBar />
            <Container>
                <Component {...pageProps} />
            </Container>
        </React.Fragment>
    )
}

export default MyApp
